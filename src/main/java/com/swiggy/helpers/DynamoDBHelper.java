package com.swiggy.helpers;

import com.amazonaws.regions.Regions;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.swiggy.helpers.ruleEngine.RuleEntity;
import com.swiggy.kvalue.DataAccessFacade;
import com.swiggy.kvalue.DataDefinitionFacade;
import com.swiggy.kvalue.data.serializer.StringDataSerializer;
import com.swiggy.kvalue.dynamodb.DDBClientProvider;
import com.swiggy.kvalue.dynamodb.DDBConnectionProperties;
import com.swiggy.kvalue.dynamodb.DDBTablePropertiesProvider;
import com.swiggy.kvalue.pojo.Key;
import com.swiggy.kvalue.pojo.schema.Schema;

import java.io.IOException;
import java.util.Optional;

public class DynamoDBHelper {

    private TableDetails table;

    private DataAccessFacade<String> dataAccessFacade;

    private DataDefinitionFacade dataDefinitionFacade;

    private ObjectMapper mapper = new ObjectMapper();

    public DynamoDBHelper(TableDetails table,
                          DataAccessFacade<String> dataAccessFacade,
                          DataDefinitionFacade dataDefinitionFacade) {
        this.table = table;
        this.dataAccessFacade = dataAccessFacade;
        this.dataDefinitionFacade = dataDefinitionFacade;
    }

    public static void main(String[] args) throws IOException {
        DDBConnectionProperties props = new DDBConnectionProperties("fak", "fas",
                Regions.AP_SOUTHEAST_1);
        props.setEndpointURL("http://localhost:8000");
        DDBClientProvider provider = new DDBClientProvider(props);
        DataAccessFacade<String> dataAccessFacade = new DataAccessFacade(provider,
                new StringDataSerializer(), String.class);
        DataDefinitionFacade dataDefinitionFacade = new DataDefinitionFacade(provider);
        DynamoDBHelper dbHelper = new DynamoDBHelper(
                new TableDetails("finance-tns_rules", "id", "version"), dataAccessFacade,
                dataDefinitionFacade);

//        dbHelper.createTable();

//        RuleEntity ruleEntity = TestUtils.createRule("rule-with-bean");
//        dbHelper.putBlob("rule-2", "0", "ruleEntity", ruleEntity);

        System.out.println(dbHelper.getBlob("rule-2", "0", "ruleEntity", RuleEntity.class));
    }

    public void createTable() {
        Key key = new Key(table.partitionKey, Optional.of(table.sortKey));

        Schema schema = new Schema(table.name, key);
        dataDefinitionFacade.createTableIfNotExists(schema,
                new DDBTablePropertiesProvider(1L, 1L));

        dataDefinitionFacade.validateTable(schema);
    }

    public void putBlob(String partitionKey, String sortKey, String blobName, Object item) throws
            JsonProcessingException {
        Key key = new Key(table.partitionKey, partitionKey, Optional.of(table.sortKey),
                Optional.of(sortKey));
        String blobStr = mapper.writeValueAsString(item);
        dataAccessFacade.putBlob(table.name, key, blobName, blobStr);
    }

    public void deleteBlob(Key key, String blobName) {

    }

    public <T> T getBlob(String partitionKey, String sortKey, String blobName, Class<T> claz) throws
            IOException {
        Key key = new Key(table.partitionKey, partitionKey, Optional.of(table.sortKey),
                Optional.of(sortKey));
        Optional<String> item = dataAccessFacade.getBlob(table.name, key, blobName);
        if (item.isPresent()) {
            return mapper.readValue(item.get(), claz);
        } else {
            System.out.println("Not found!");
            return null;
        }
    }

    private static class TableDetails {

        public String name;
        public String partitionKey;
        public String sortKey;

        public TableDetails(String name, String partitionKey, String sortKey) {
            this.name = name;
            this.partitionKey = partitionKey;
            this.sortKey = sortKey;
        }

    }
}
