package com.swiggy.helpers.ruleEngine;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.swiggy.helpers.ruleEngine.constant.ExpressionLang;
import com.swiggy.helpers.ruleEngine.constant.RuleNodeType;
import lombok.Data;

import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class RuleNode {

    @JsonProperty(value = "type", required = true)
    private RuleNodeType type;

    @JsonProperty("content")
    private String content;

    @JsonProperty("resultAttr")
    private String resultAttr;

    @JsonProperty("lang")
    private ExpressionLang lang;

    @JsonProperty("onTrue")
    private List<RuleNode> onTrue;

    @JsonProperty("onFalse")
    private List<RuleNode> onFalse;
}


