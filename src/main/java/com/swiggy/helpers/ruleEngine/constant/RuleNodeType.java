package com.swiggy.helpers.ruleEngine.constant;

public enum RuleNodeType {
    START, CONDITION, ACTION, END;
}
