package com.swiggy.helpers.ruleEngine.constant;

public enum ExpressionLang {
    SPEL, MVEL;
}
