package com.swiggy.helpers.ruleEngine;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

public class TestUtils {

    private static ObjectMapper mapper = new ObjectMapper();

    public static RuleEntity createRule(String ruleId) throws IOException {
        return mapper.readValue(TestUtils.class.getClassLoader().getResourceAsStream(
                "rules/" + ruleId + ".txt"), RuleEntity.class);
    }
}
