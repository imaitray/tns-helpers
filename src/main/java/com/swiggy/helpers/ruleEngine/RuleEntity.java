package com.swiggy.helpers.ruleEngine;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.time.Instant;
import java.util.Map;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class RuleEntity {

    @JsonProperty(value = "id", required = true)
    private String id;

    @JsonProperty(value = "version", required = true)
    private String version;

    @JsonProperty("createdTs")
    private Instant createdTs;

    @JsonProperty("createdBy")
    private Map<String, String> createdBy;

    @JsonProperty(value = "ruleNode", required = true)
    private RuleNode ruleNode;
}
